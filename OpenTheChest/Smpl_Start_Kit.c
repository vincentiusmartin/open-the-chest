/*---------------------------------------------------------------------------------------------------------*/
/*                                                                                                         */
/* Copyright (c) Nuvoton Technology Corp. All rights reserved.                                             */
/*                                                                                                         */
/*---------------------------------------------------------------------------------------------------------*/

#include <stdio.h>
#include "NUC1xx.h"
#include "Driver\DrvSYS.h"
#include "Driver\DrvGPIO.h"
#include "LCD_Driver.h"
#include "Seven_Segment.h"
#include "ScanKey.h"
#include "EEPROM_24LC64.h"

void delay_loop(void)
 {
 uint32_t i,j;
	for(i=0;i<3;i++)	
	{
		for(j=0;j<600;j++);
    }
 
 }

/*----------------------------------------------------------------------------
  Interrupt subroutine
  ----------------------------------------------------------------------------*/
static unsigned char count=0;
static unsigned char loop=12;
void TMR0_IRQHandler(void) // Timer0 interrupt subroutine 
{ 
    unsigned char i=0;
 	TIMER0->TISR.TIF =1;
	count++;
	if(count==5)
	{
	   	DrvGPIO_ClrBit(E_GPC,loop);
	   	loop++;
	   	count=0;
	   	if(loop==17)
	   	{
	   		for(i=12;i<16;i++)
		   	{
	   			DrvGPIO_SetBit(E_GPC,i);	   
	   		}
			loop=12;
	   }
	}
}

void Timer_initial(void)
{
	/* Step 1. Enable and Select Timer clock source */          
	SYSCLK->CLKSEL1.TMR0_S = 0;	//Select 12Mhz for Timer0 clock source 
    SYSCLK->APBCLK.TMR0_EN =1;	//Enable Timer0 clock source

	/* Step 2. Select Operation mode */	
	TIMER0->TCSR.MODE=1;		//Select periodic mode for operation mode

	/* Step 3. Select Time out period = (Period of timer clock input) * (8-bit Prescale + 1) * (24-bit TCMP)*/
	TIMER0->TCSR.PRESCALE=0;	// Set Prescale [0~255]
	TIMER0->TCMPR  = 1000000;		// Set TICR(TCMP) [0~16777215]
								// (1/22118400)*(0+1)*(2765)= 125.01usec or 7999.42Hz

	/* Step 4. Enable interrupt */
	TIMER0->TCSR.IE = 1;
	TIMER0->TISR.TIF = 1;		//Write 1 to clear for safty		
	NVIC_EnableIRQ(TMR0_IRQn);	//Enable Timer0 Interrupt

	/* Step 5. Enable Timer module */
	TIMER0->TCSR.CRST = 1;		//Reset up counter
	TIMER0->TCSR.CEN = 1;		//Enable Timer0

  	TIMER0->TCSR.TDR_EN=1;		// Enable TDR function
}		  

//save input to buffer
//static char buf[4];

void resetMemoryProgress(void){
	Write_24LC64(0,1);
}

char toHex(int input){
	//DICTIONARY
	unsigned char convert[17] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F', ' '}; 
	//ALGORITHM
	if(input < 17){
		return convert[input];
	}else{
		return '0';
	}
}

void printLCD(int currentpage){
	//DICTIONARY
	int lastposition = Read_24LC64(0);
	char a = ' ',b = ' ',c = ' ',d = ' ',e = ' ',f = ' ',g = ' ',h = ' ';
	char toprint[9];
	memset(toprint, ' ', 9);
	//ALGORITHM
	print_lcd(3,"                                   ");
	if(currentpage == 0){
		print_lcd(0, "Current Keys : ");	  

		//lcd 1
		if(lastposition > currentpage){
			if(lastposition > (currentpage + 1)){
				a = toHex(Read_24LC64(currentpage + 1));
			}
			if(lastposition > (currentpage + 2)){
				b = toHex(Read_24LC64(currentpage + 2));
			}
			if(lastposition > (currentpage + 3)){
				c = toHex(Read_24LC64(currentpage + 3));
			}
			if(lastposition > (currentpage + 4)){
				d = toHex(Read_24LC64(currentpage + 4));
			}
			if(lastposition > (currentpage + 5)){
				e = toHex(Read_24LC64(currentpage + 5));
			}
			if(lastposition > (currentpage + 6)){
				f = toHex(Read_24LC64(currentpage + 6));
			}
			if(lastposition > (currentpage + 7)){
				g = toHex(Read_24LC64(currentpage + 7));
			}
			if(lastposition > (currentpage + 8)){
				h = toHex(Read_24LC64(currentpage + 8));
			}
		}
		sprintf(toprint,"%c%c%c%c %c%c%c%c",a,b,c,d,e,f,g,h);

		print_lcd(1, toprint);
	
		//make empty
		a = ' ',b = ' ',c = ' ',d = ' ',e = ' ',f = ' ',g = ' ',h = ' ';		

		//lcd 2
		if(lastposition > currentpage){
			if(lastposition > (currentpage + 9)){
				a = toHex(Read_24LC64(currentpage + 9));
			}
			if(lastposition > (currentpage + 10)){
				b = toHex(Read_24LC64(currentpage + 10));
			}
			if(lastposition > (currentpage + 11)){
				c = toHex(Read_24LC64(currentpage + 11));
			}
			if(lastposition > (currentpage + 12)){
				d = toHex(Read_24LC64(currentpage + 12));
			}
			if(lastposition > (currentpage + 13)){
				e = toHex(Read_24LC64(currentpage + 13));
			}
			if(lastposition > (currentpage + 14)){
				f = toHex(Read_24LC64(currentpage + 14));
			}
			if(lastposition > (currentpage + 15)){
				g = toHex(Read_24LC64(currentpage + 15));
			}
			if(lastposition > (currentpage + 16)){
				h = toHex(Read_24LC64(currentpage + 16));
			}
		}
		sprintf(toprint,"%c%c%c%c %c%c%c%c",a,b,c,d,e,f,g,h);


		print_lcd(2, toprint);	
	}else{
		//empty lcd
		print_lcd(0,"                                   ");
		print_lcd(1,"                                   ");
		print_lcd(2,"                                   ");

		//lcd 0
		if(lastposition > currentpage){
			if(lastposition > (16 + ((currentpage-1)*24) + 1)){
				a = toHex(Read_24LC64(16 + ((currentpage - 1) * 24) + 1));
			}
			if(lastposition > (16 + ((currentpage-1)*24) + 2)){
				b = toHex(Read_24LC64(16 + ((currentpage - 1) * 24) + 2));
			}
			if(lastposition > (16 + ((currentpage-1)*24) + 3)){
				c = toHex(Read_24LC64(16 + ((currentpage - 1) * 24) + 3));
			}
			if(lastposition > (16 + ((currentpage-1)*24) + 4)){
				d = toHex(Read_24LC64(16 + ((currentpage - 1) * 24) + 4));
			}
			if(lastposition > (16 + ((currentpage-1)*24) + 5)){
				e = toHex(Read_24LC64(16 + ((currentpage - 1) * 24) + 5));
			}
			if(lastposition > (16 + ((currentpage-1)*24) + 6)){
				f = toHex(Read_24LC64(16 + ((currentpage - 1) * 24) + 6));
			}
			if(lastposition > (16 + ((currentpage-1)*24) + 7)){
				g = toHex(Read_24LC64(16 + ((currentpage - 1) * 24) + 7));
			}
			if(lastposition > (16 + ((currentpage-1)*24) + 8)){
				h = toHex(Read_24LC64(16 + ((currentpage - 1) * 24) + 8));
			}
		}
		sprintf(toprint,"%c%c%c%c %c%c%c%c",a,b,c,d,e,f,g,h);

		print_lcd(0, toprint);
	
		//make empty
		a = ' ',b = ' ',c = ' ',d = ' ',e = ' ',f = ' ',g = ' ',h = ' ';		

		//lcd 1
		if(lastposition > currentpage){
			if(lastposition > (16 + ((currentpage-1)*24) + 9)){
				a = toHex(Read_24LC64(16 + ((currentpage - 1) * 24) + 9));
			}
			if(lastposition > (16 + ((currentpage-1)*24) + 10)){
				b = toHex(Read_24LC64(16 + ((currentpage - 1) * 24) + 10));
			}
			if(lastposition > (16 + ((currentpage-1)*24) + 11)){
				c = toHex(Read_24LC64(16 + ((currentpage - 1) * 24) + 11));
			}
			if(lastposition > (16 + ((currentpage-1)*24) + 12)){
				d = toHex(Read_24LC64(16 + ((currentpage - 1) * 24) + 12));
			}
			if(lastposition > (16 + ((currentpage-1)*24) + 13)){
				e = toHex(Read_24LC64(16 + ((currentpage - 1) * 24) + 13));
			}
			if(lastposition > (16 + ((currentpage-1)*24) + 14)){
				f = toHex(Read_24LC64(16 + ((currentpage - 1) * 24) + 14));
			}
			if(lastposition > (16 + ((currentpage-1)*24) + 15)){
				g = toHex(Read_24LC64(16 + ((currentpage - 1) * 24) + 15));
			}
			if(lastposition > (16 + ((currentpage-1)*24) + 16)){
				h = toHex(Read_24LC64(16 + ((currentpage - 1) * 24) + 16));
			}
		}
		sprintf(toprint,"%c%c%c%c %c%c%c%c",a,b,c,d,e,f,g,h);

		print_lcd(1, toprint);
		
		//make empty
		a = ' ',b = ' ',c = ' ',d = ' ',e = ' ',f = ' ',g = ' ',h = ' ';

		//lcd 2
		if(lastposition > currentpage){
			if(lastposition > (16 + ((currentpage-1)*24) + 17)){
				a = toHex(Read_24LC64(16 + ((currentpage - 1) * 24) + 17));
			}
			if(lastposition > (16 + ((currentpage-1)*24) + 18)){
				b = toHex(Read_24LC64(16 + ((currentpage - 1) * 24) + 18));
			}
			if(lastposition > (16 + ((currentpage-1)*24) + 19)){
				c = toHex(Read_24LC64(16 + ((currentpage - 1) * 24) + 19));
			}
			if(lastposition > (16 + ((currentpage-1)*24) + 20)){
				d = toHex(Read_24LC64(16 + ((currentpage - 1) * 24) + 20));
			}
			if(lastposition > (16 + ((currentpage-1)*24) + 21)){
				e = toHex(Read_24LC64(16 + ((currentpage - 1) * 24) + 21));
			}
			if(lastposition > (16 + ((currentpage-1)*24) + 22)){
				f = toHex(Read_24LC64(16 + ((currentpage - 1) * 24) + 22));
			}
			if(lastposition > (16 + ((currentpage-1)*24) + 23)){
				g = toHex(Read_24LC64(16 + ((currentpage - 1) * 24) + 23));
			}
			if(lastposition > (16 + ((currentpage-1)*24) + 24)){
				h = toHex(Read_24LC64(16 + ((currentpage - 1) * 24) + 24));
			}
		}
		sprintf(toprint,"%c%c%c%c %c%c%c%c",a,b,c,d,e,f,g,h);


		print_lcd(2, toprint);		
	}			
}

int main(void)
{	
	int i,j = 0,drawNow = 0,inputNow=-1;
	int a =16,b =16,c = 16,d = 16;
	int temp = 0;
	int boolean = 0;
	int delay = 0;
	//initialize button value
	int btn = 0;
	int counter = 0;
	int lastBoolean = 0;
	int	delete = 0;

	int currentpage = 0;
	int lastpage;
	int regiterator;
	/* Unlock the protected registers */	
	UNLOCKREG();													 
   	/* Enable the 12MHz oscillator oscillation */
	DrvSYS_SetOscCtrl(E_SYS_XTL12M, 1);
 
     /* Waiting for 12M Xtal stalble */
    SysTimerDelay(5000);					
 
	/* HCLK clock source. 0: external 12MHz; 4:internal 22MHz RC oscillator */
	DrvSYS_SelectHCLKSource(0);		
    /*lock the protected registers */
	LOCKREG();				

	DrvSYS_SetClockDivider(E_SYS_HCLK_DIV, 0); /* HCLK clock frequency = HCLK clock source / (HCLK_N + 1) */

    for(i=12;i<16;i++)
	{		
		DrvGPIO_Open(E_GPC, i, E_IO_OUTPUT);
    }

	Initial_pannel();  //call initial pannel function
	clr_all_pannal();
  	  
	Timer_initial();
	
	//init eeprom
	DrvGPIO_InitFunction(E_FUNC_I2C1);

	//print first screen
	printLCD(currentpage);

	while(1)
	{
		//count total page
		if(Read_24LC64(0) < 5){
			lastpage = 0;
		}else{
			//minimum page is 1
			lastpage = 1 + ((Read_24LC64(0) - 16) / 24);
		}

		temp = 16;
		boolean = 0;
		delete  = 0;
		/*for(i=0;i<9;i++)
		{
		  for(j=0;j<4;j++)
		  {
			  close_seven_segment();
			  show_seven_segment(j,i);
			  delay_loop();
		  }
		}	*/	
			  
		OpenKeyPad();
			switch(Scankey()){									 
				case 1 :
					boolean = 1;
					if (lastBoolean == 0){
						if (counter != 1){
							counter = 1;
							btn = 0;
							temp = 0;
							inputNow++;
						}else{
							btn++;
							if(btn > 1){
								btn = 0;
							}
							if (btn == 0){
								temp = 0;	
							}else{
								temp = 1;
							}	
						}
					}
				break;
				case 2 :
					boolean = 1;
					if (lastBoolean == 0){
						if (counter != 2){
							counter = 2;
							btn = 0;
							temp = 2;
							inputNow++;
						}else{
							btn++;
							if(btn > 2){
								btn = 0;
							}
							if (btn == 0){
								temp = 2;	
							}else if (btn == 1){
								temp = 3;	
							}else{
								temp = 4;
							}	
						}
					}
				break;
				case 3 :
					boolean = 1;
					if (lastBoolean == 0){
						if (counter != 3){
							counter = 3;
							btn = 0;
							temp = 5;
							inputNow++;
						}else{
							btn++;
							if(btn > 2){
								btn = 0;
							}
							if (btn == 0){
								temp = 5;	
							}else if (btn == 1){
								temp = 6;	
							}else{
								temp = 7;
							}	
						}
					}
				break;
				case 4 :
					boolean = 1;
						if (lastBoolean == 0){
						if (counter != 4){
							counter = 4;
							btn = 0;
							temp = 8;
							inputNow++;
						}else{
							btn++;
							if(btn > 1){
								btn = 0;
							}
							if (btn == 0){
								temp = 8;	
							}else{
								temp = 9;
							}	
						}
					}
				break;
				case 5 :
					boolean = 1;
						if (lastBoolean == 0){
						if (counter != 5){
							counter = 5;
							btn = 0;
							temp = 10;
							inputNow++;
						}else{
							btn++;
							if(btn > 2){
								btn = 0;
							}
							if (btn == 0){
								temp = 10;	
							}else if (btn == 1){
								temp = 11;	
							}else{
								temp = 12;
							}	
						}
					}
				break;
				case 6 :
					boolean = 1;
					if (lastBoolean == 0){
						if (counter != 6){
							counter = 6;
							btn = 0;
							temp = 13;
							inputNow++;
						}else{
							btn++;
							if(btn > 2){
								btn = 0;
							}
							if (btn == 0){
								temp = 13;	
							}else if (btn == 1){
								temp = 14;	
							}else{
								temp = 15;
							}	
						}
					}
				break;
				case 7:
					boolean = 1;
					if (lastBoolean == 0){
						counter = 7;
						temp = 16;
						delete = 1;
					}
				break;
				case 8:
					boolean = 1;
					if (lastBoolean == 0){	
						if(counter != 8){
							//save into register
							counter = 8;
							delay = 0;
							regiterator = Read_24LC64(0);
							
							Write_24LC64(regiterator,a);
							regiterator++;
							Write_24LC64(regiterator,b);
							regiterator++;
							Write_24LC64(regiterator,c);
							regiterator++;			  
							Write_24LC64(regiterator,d);
							regiterator++;
		
							Write_24LC64(0,regiterator);
		
							//print into lcd
							printLCD(currentpage);
						}else{
							//reset progress
							inputNow = -1;
							a = 16;
							b = 16;
							c = 16;
							d = 16;
		
							resetMemoryProgress();
							regiterator = Read_24LC64(0);
							print_lcd(1,"                                      ");
							print_lcd(2,"                                      ");
							print_lcd(3,"reset");
						}  
					}
				break;
				case 9:
					boolean = 1;
					if (lastBoolean == 0){
						 counter = 9;
						 currentpage++;
						 if(currentpage > lastpage){
						 	currentpage = 0;	
						 }
						 //print into lcd
						 printLCD(currentpage);
					}						 
				break;
			}
		

		if (lastBoolean == 0 && boolean == 1 && (counter <8)){
			delay = 0;
			switch (inputNow){
				case 0:
					a = temp;
				break;
				case 1:
					b = temp;
				break;
				case 2:
					c = temp;								
				break;
				case 3:
					d = temp;
				break;
			}
		}

		if (inputNow > 3){
			inputNow = 3;
		}

		if (delete  == 1){
			inputNow--;
			if (inputNow < -1){
				inputNow = -1;
			}
		} 
					   
		//for (j = 0; j< 3;j++){
			drawNow++;
			drawNow = drawNow%4;
			switch(drawNow){
				case 0 :
					close_seven_segment();
					show_seven_segment(3,a);
				break;
				case 1 :
					close_seven_segment();
					show_seven_segment(2,b);
				break;
				case 2 :
					close_seven_segment();
					show_seven_segment(1,c);
				break;
				case 3 :
					close_seven_segment();
					show_seven_segment(0,d);
				break;
			}		
		//}
		
		lastBoolean = boolean;
		delay++;
		if (delay > 500){
			counter = 0;
		}
		CloseKeyPad();	

		//EEPROM and LED - 0 to save last iteration	, temp
		/*if(boolean == 1){
			//Write_24LC64(0,1);
			//resetMemoryProgress();
			sprintf(buf,"%d",Read_24LC64(0));
			//sprintf(buf,"%c%c%c%c",toHex(Read_24LC64(1)),toHex(Read_24LC64(2)),toHex(Read_24LC64(3)),toHex(Read_24LC64(4)));
			print_lcd(0, "hai");
			print_lcd(1, buf);
		} */
		
	}
		  				
}
